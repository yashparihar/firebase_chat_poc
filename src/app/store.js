import {applyMiddleware, combineReducers, createStore} from "redux";
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';

import userReducer from './reducer/userReducer';
import loginReducer from './reducer/loginReducer';
import chatboxReducer from './reducer/chatboxReducer';


export default createStore(combineReducers({
        user: userReducer,
        login: loginReducer,
        chat: chatboxReducer
    }),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(logger, promise(), thunk)
);