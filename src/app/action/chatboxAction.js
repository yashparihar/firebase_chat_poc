import { create } from "domain";
import { getData, pushInFbDb } from './../utility/helper.js';

//NEED TO MAKE IT WORK ASYN
// MERGE BOTH USER HISTORY AND ORDER BY TIME AND ADDING TAG OF 'BY'
export function startChat(user, otheruser, otheruserKey) {
    // METHOD 3 : CREATING INDIVIDUAL CHAT FOR USER AND OTHERUSER

    const createChat = {
        id: user + otheruser,
        chatHistory: []
    }

    let userkey = localStorage.getItem('userkey');
    //  let urle = '/users/' + userkey + '/chatlist/' + otheruser;
    let url_a = '/users/' + otheruserKey + '/chatlist/' + user;
    let ref = firebase.database().ref(url_a);
    let otheruser_chatkey = null;
    let chatkey = null;

    return dispatch => {

        // CHECK IF USERB HAS CHAT WITH USERA
        ref.once('value', (snapshot) => {
            let chatkey = snapshot.val();
            console.log(url_a,chatkey);
            if (chatkey) {
                // CHAT EXIST OF USERB WITH USERA
                console.log('CHAT EXIST OF USERB WITH USERA', chatkey);
                otheruser_chatkey = chatkey;
                localStorage.setItem('otheruser_chatkey', otheruser_chatkey);


                // CHECK IF USERA HAS CHAT WITH USERB..................
                let url_c = '/users/' + userkey + '/chatlist/' + otheruser
                firebase.database().ref(url_c).once('value', (snapshot) => {
                    chatkey = snapshot.val();
                    if (chatkey) {
                        console.log('CHAT EXIST OF USERA WITH USERB',chatkey);

                       // console.log('--> user chat with userb exist', result.chatkey);
                        localStorage.setItem('chatKey', chatkey);

                        setTimeout(function(){ 
                            dispatch(startChatDone(otheruser, otheruserKey));
                       }, 0);

                    } else {

                        // DONT EXSIT THEN CREATE CHAT AND STORE CHATKEEY IN USERA CHATLIST
                        pushInFbDb(createChat, '/chats', 'chatKey').then(() => {
                            console.log('CHAT dont EXIST OF USERA WITH USERB');
                           
                            chatkey = localStorage.getItem('chatKey');
                            let url_d = "/users/" + userkey + '/chatlist/' + otheruser;
                            firebase.database().ref(url_d).set(chatkey);

                            setTimeout(function(){ 
                                dispatch(startChatDone(otheruser, otheruserKey));
                           }, 0);
                        })

                    }
                })
                // ..... USERA CHAT......................



            } else {

                // DONT EXIST THEN CREATE CHAT AND STORE KEY IN USERB CHATLIST
                pushInFbDb(createChat, '/chats', 'otheruser_chatkey').then(() => {

                    console.log('CHAT dont EXIST OF USERB WITH USERA');

                    otheruser_chatkey = localStorage.getItem('otheruser_chatkey');
                    let url_b = "/users/" + otheruserKey + '/chatlist/' + user;
                    firebase.database().ref(url_b).set(otheruser_chatkey);



                    // CHECK IF USERA HAS CHAT WITH USERB..................
                    let url_c = '/users/' + userkey + '/chatlist/' + otheruser
                    firebase.database().ref(url_c).once('value', (snapshot) => {
                        chatkey = snapshot.val();
                        console.log(url_c, chatkey);
                        if (chatkey) {
                            console.log('Exist USERA HAS CHAT WITH USERB', chatkey);
                            localStorage.setItem('chatKey', chatkey);

                            setTimeout(function(){ 
                                dispatch(startChatDone(otheruser, otheruserKey));
                           }, 0);

                        } else {

                            // DONT EXSIT THEN CREATE CHAT AND STORE CHATKEEY IN USERA CHATLIST
                            pushInFbDb(createChat, '/chats', 'chatKey').then(() => {
                                console.log('DONT EXIsT THEN CREATE CHAT AND STORE CHATKEEY IN USERA CHATLIST');

                                chatkey = localStorage.getItem('chatKey');
                                let url_d = "/users/" + userkey + '/chatlist/' + otheruser;
                                firebase.database().ref(url_d).set(chatkey);

                                setTimeout(function(){ 
                                    dispatch(startChatDone(otheruser, otheruserKey));
                               }, 0);
                            })

                        }
                    })
                    // ..... USERA CHAT......................


                });
            }
        })
    }


}


function startChatDone(otheruser, otheruserKey) {
    return {
        type: "START_CHAT",
        payload: {
            user: otheruser,
            userKey: otheruserKey
        }
    }
}



export function uploadFile(user, otheruser, otheruserKey, file, msg) {
    // 1 . CALLES FIREBASE STORAGE TO STORE IN IT
    // 2. GET METADATA, KET,URL FROM STORAGE REF 
    // 3. THEN DISPATCH SENDMSG WITH ABOVE DETAILS

    var storageRef = firebase.storage().ref('chat_storage');
    var uploadsMetadata = {
        cacheControl: "max-age=" + (60 * 60 * 24 * 365) // One year of seconds
    };

    var uploadTask = storageRef.child(file.name).put(file, uploadsMetadata);


    return dispatch => {

        uploadTask.on('state_changed', function (snap) {
            console.log('Progress: ', snap.bytesTransferred, '/', snap.totalBytes, ' bytes');
        },
            function (err) {
                console.log('upload error', err);
                reject(err);
            },
            function () {
                console.log('File det-->', uploadTask.snapshot);

                var metadata = uploadTask.snapshot.metadata;
                var key = metadata.md5Hash.replace(/\//g, ':');
                var fileRecord = {
                    //   downloadURL: uploadTask.uploadUrl_ , 
                    key: key,
                    caption: msg,
                    metadata: {
                        fullPath: metadata.fullPath,
                        md5Hash: metadata.md5Hash,
                        type: metadata.contentType,
                        name: metadata.name
                    }
                };
                console.log('file fb storage det:', uploadTask, metadata)
                dispatch(sendMsg(user, otheruser, otheruserKey, fileRecord));
            });
    }


}


export function sendMsg(user, otheruser, otheruserKey, msg) {

 // METHOD 3'
 let currenttime = new Date().getTime() / 1000;
 const sendChat = {
    by: user,
    time: currenttime,
    message: msg
}

 let userkey = localStorage.getItem('userkey');
 //  let urle = '/users/' + userkey + '/chatlist/' + otheruser;
 let url_a = '/users/' + otheruserKey + '/chatlist/' + user;
 let ref = firebase.database().ref(url_a);
 let otheruser_chatkey = null;
 let mychatkey = localStorage.getItem('chatKey');  


 return dispatch => {

    // CHECK IF USERB HAS CHAT WITH USERA
    ref.once('value', (snapshot) => {
        let chatkey = snapshot.val();
        console.log(url_a,chatkey);
        if (chatkey) {
            // CHAT EXIST OF USERB WITH USERA
            console.log('CHAT EXIST OF USERB WITH USERA', chatkey);
            otheruser_chatkey = chatkey;
            localStorage.setItem('otheruser_chatkey', otheruser_chatkey);


            // SET MESSAGES..................
            const url = 'chats/' + mychatkey + '/chatHistory';
            addlistener(user, otheruserKey);
            // WHAT IF SENDING MESSAGES FAILS
       
            pushInFbDb(sendChat, url, null).then(
                () => {
                     console.log('msg send sucessfully to my chat');
                     const url_otheruser = 'chats/' + otheruser_chatkey + '/chatHistory';
                     
                     pushInFbDb(sendChat, url_otheruser, null).then(
                        () => {
                            console.log('msg send sucessfully to otheruser chat');
                            dispatch(sendMsgDone(user, msg, chatkey));
                        })

                })
               .catch((err) => { console.log('error in sending msg', err) })
                 
            //.. END .. SET MESSAGES .........


        } else {
            const createChat = {
                id: user + otheruser,
                chatHistory: []
            }
            // DONT EXIST THEN CREATE CHAT AND STORE KEY IN USERB CHATLIST
            pushInFbDb(createChat, '/chats', 'otheruser_chatkey').then(() => {

                console.log('CHAT dont EXIST OF USERB WITH USERA');

                otheruser_chatkey = localStorage.getItem('otheruser_chatkey');
                let url_b = "/users/" + otheruserKey + '/chatlist/' + user;
                firebase.database().ref(url_b).set(otheruser_chatkey);

                        // SET MESSAGES..................
                    const url = 'chats/' + mychatkey + '/chatHistory';
                    addlistener(user, otheruserKey);
                    // WHAT IF SENDING MESSAGES FAILS
            
                    pushInFbDb(sendChat, url, null).then(
                        () => {
                            console.log('msg send sucessfully to my chat');
                            const url_otheruser = 'chats/' + otheruser_chatkey + '/chatHistory';
                            
                            pushInFbDb(sendChat, url_otheruser, null).then(
                                () => {
                                    console.log('msg send sucessfully to otheruser chat');
                                    dispatch(sendMsgDone(user, msg, chatkey));
                                })

                        })
                    .catch((err) => { console.log('error in sending msg', err) })
                        
                    //.. END .. SET MESSAGES .........


            });
        }
    })
}


}


function sendMsgDone(user, msg, ckey) {
    return {
        type: "SEND_MSG",
        payload: {
            message: msg,
            byUser: user,
            chatKey: ckey
        }
    }
}


// ADDING LISTENER
function addlistener(userid, userkey) {
    let url = 'users/' + userkey + '/listener/';
    let ref_b = firebase.database().ref(url + userid);
    ref_b.once('value', (snapshot) => {
        let result = snapshot.val();
        console.log('SUCESS In getting listener path', result);

        if (result === null) {  // ADD USERID AND TOTAL MESSAGE TO 1
            console.log("--> SHOws NULL");
            let payload = {
                totmsg: 1
            }
            let ref_done = firebase.database().ref(url).child(userid).set(payload);
            //console.log('After putting listener ',ref_done);
        } else {
            console.log("--> dont show NULL");
            let payload = {
                totmsg: result.totmsg + 1
            }
            ref_b.update(payload);
        }

    },
        (error) => {
            console.log("Error: @ addlistener", error);
        })

}
// END.. ADDING LISTENER

export function cleanListener(userkey, otherUser) {
    let listener_url = 'users/' + userkey + '/listener/' + otherUser + '/totmsg';
    console.log('listener url', listener_url);
    let ref_done = firebase.database().ref(listener_url).set(0);
    console.log('cleanlistener ', ref_done);
    return dispatch => {
        ref_done.then(() => {
            console.log('done putting zero');
            dispatch(cleanListenerDone());
        })
    }
}

function cleanListenerDone() {
    return {
        type: "CLEAN_LISTENER"
    }
}

export function hideChat() {
    return {
        type: "HIDE_CHAT",
        payload: {

        }
    }
}


// THIS WILL CLEAR ALL STATE DETAILS ONLY FROM REDUCER
export function closeChat() {
    return (dispatch, getState) => {
        let ref = getState().chat.firebaseRef_chat;
        if (ref) ref.off();
        dispatch(closeChatDone());
    }
}

export function closeChatDone() {
    return {
        type: "CLOSE_CHAT",
    }
}

// THIS WILL CLEAR ALL STATE DETAILS ONLY FROM REDUCER
export function deleteMessage(msgkey) {

    // LOGIC TO REMOVE A MESSAGE FROM A CHAT USING GIVEN MSG KEY
    return dispatch => {
        const chatkey = localStorage.getItem('chatKey');
        console.log('==> key: ', msgkey);

        const url = 'chats/' + chatkey + '/chatHistory/' + msgkey;

        let ref = firebase.database().ref(url);
        ref.remove().then(() => {
            dispatch(deleteMessageDone());
        });
    }
}

export function deleteMessageDone() {
    return {
        type: "DELETE_MESSAGE_DONE",
    }
}

export function refresh() {
    return {
        type: "REFRESH",
    }
}


// THIS WILL CLEAR ALL STATE DETAILS AS WELL HISTORT FROM SERVER DB
export function deleteChat() {
    return {
        type: "DELETE_CHAT",
    }
}



/*
    Dispatch event : for populating messages in the chatbox
    
*/
export function getMessages() {

    /* CALL FIREBASE  */
}

function getMessagesDone(msg) {

    return {
        type: 'GET_MESSAGES',
        payload: {
            messages: msg
        }
    }
}