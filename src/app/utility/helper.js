import _ from 'lodash';

export function getData(values) {
    let messagesVal = values;
    let messages = _(messagesVal)
        .keys()
        .map(messageKey => {
            let cloned = _.clone(messagesVal[messageKey]);
            cloned.key = messageKey;
            return cloned;
        })
        .value();
    //   this.setState({
    //     message: messages
    //   });
    return messages;
}


export function pushInFbDb(payload, url, storageKey) {
    //  firebase.database().ref('/chat'+nextMsg.id).set(nextMsg)
  //  console.log('chat flg ', isChatKey);

    return new Promise((resolve, reject) => {

        try {
            let ref = firebase.database().ref(url);
            // this new, empty ref only exists locally
            let newChildRef = ref.push();
            // we can get its id using key()
            console.log('my new shiny id is ' + newChildRef.key);

            if (storageKey)
                localStorage.setItem(storageKey, newChildRef.key)

            // now it is appended at the end of data at the server
            resolve(newChildRef.set(payload));
        }
        catch (err) {
            reject(err);
        }

    });
}

export function getfromFbDb() {

}