const chatboxReducer = (state = {

    otherUser: null,                  // OTHER USER ID
    otherUser_key: null,              // OTHER USER FIREBASE KEY TO ACCESS ITS LISTNER,CHATS
    chatHistory: [{ msg: 'nothing' }],
    boxStatus: false,
    firebaseRef_chat: null,           // LISTENING TO CURRENT CHAT  
    chatKey: null,

}, action) => {
    switch (action.type) {
        case "START_CHAT":
            state = {
                ...state,
                otherUser: action.payload.user,
                otherUser_key: action.payload.userKey,
                boxStatus: true,
                chatKey: localStorage.getItem('chatKey'),
                firebaseRef_chat: firebase.database().ref('chats/' + localStorage.getItem('chatKey') + '/chatHistory')
            }
            break;
        case "SEND_MSG":
            // PAYLOAD:MSG 
            // APPEND (MSG,TIME,BY:USER) IN CHATHISTORY
            console.log('send msg in REDUCER ', state.chatHistory);
            state = {
                ...state,
                chatKey: action.payload.chatKey,
                // chatHistory: [...state.chatHistory,
                // {
                //     message: action.payload.message,
                //     time: new Date(),
                //     by: action.payload.byUser
                // }]

            }
            break;
        case "CLOSE_CHAT":

            state = {
                ...state,
                otherUser: null,
                otherUser_key: null,
                chatHistory: [{ msg: 'nothing' }],
                boxStatus: false,
                firebaseRef_chat: null,
                chatKey: null,
            }
            break;

        case "CLEAN_LISTENER":
            state = {
                ...state
            }
            break;

        case "REFRESH":
            state = {
                ...state
            }
            break;

        case "DELETE_MESSAGE_DONE":
            state = {
                ...state
            }
            break;
    }
    return state;
};

export default chatboxReducer;