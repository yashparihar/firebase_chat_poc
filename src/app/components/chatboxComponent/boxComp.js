import React from "react";
import { getData } from './../../utility/helper.js'
import { CookieMap } from './../../utility/cookieLib.js';
const cookie = new CookieMap();

import { MessageComponent } from './messageComp.js'

let month = new Array(12);
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";



export class ChatBoxComponent extends React.Component {
    constructor(props) {
        super(props);
        this.updateMsg = this.updateMsg.bind(this);
        this.onChange = this.onChange.bind(this);
        this.fileValidity = this.fileValidity.bind(this);
        this.messageValidity = this.messageValidity.bind(this)


        console.log(cookie.getCookie('auth'));
        this.state = {
            chatkey: props.chat.chatKey,
            userkey: localStorage.getItem('userkey'),
            message: null,
            msgLst: null,
            chatRoom: null,
            user: cookie.getCookie('auth'),
            divStyle: {
                backgroundColor: 'grey',
                padding: '2%',
                width: '50%',
                borderStyle: 'groove',
                margin: '5%'
            },
            file: null,
            fbMsgListener: this.props.chat.firebaseRef_chat
        }
    }

    updateMsg(event) {
        this.setState({
            message: event.target.value
        });
    }

    cleanState() {
        document.getElementById('msgBox').value = null;
        document.getElementById('fileElement').value = null;
        this.setState({
            file: null,
            message: null
        })
    }

    onChange(e) {
        console.log('change file');
        this.setState({ file: e.target.files[0] })
    }

    messageValidity() {
        // CHECK IF BOTH FILE AND MSG ARE NOT NULL
        if ((this.state.message === null || this.state.message === '') && this.state.file === null) {
            alert('nothing to send');
            return false;
        }


        // IF MSG EXIST : CHECK IF CHAR < 1600 AND FOR COMMAND INJECTION
        if ((this.state.message && this.state.message !== '') && this.state.message.length > 1600) {
            console.log('MSg exist', this.state.message);
            alert('Message size limit 1600 characters')
            return false;
        }



        // IF FILE EXIST: CHECK FOR ITS VALIDITY
        if (this.state.file !== null && !this.fileValidity()) {
            return false;
        }

        return true;
    }

    fileValidity() {

        // CHECK FOR TYPE
        let ftype = this.state.file.type;

        if (ftype.indexOf('image') >= 0 || ftype.indexOf('text/plain') >= 0) {
            console.log('image type or text type');
        } else if (ftype.indexOf('application') >= 0) {
            let ext = this.state.file.name.split('.').pop();
            if (ext === 'doc' || ext === 'docx' || ext === 'pdf') { } else {
                alert('file type Not Allowed ' + ext);
                return false;
            }
        } else {
            alert('file type Invalid');
            return false;
        }

        // CHECK FOR SIZE
        let maxfilesize = 2 * 1024 * 1024; // IN BYTES
        if (this.state.file.size > maxfilesize) {
            alert('Size should be max 2Mb')
            return false;
        }

        return true;
    }


    enterpressalert(e) {
        // var code = (e.keyCode ? e.keyCode : e.which);
        if (e.key === 'Enter') { //Enter keycode
        }
    }


    componentDidMount() {
        console.log('BOX KA STATUS ----------', this.props.chat.boxStatus, this.props.chat.otherUser)

        console.log("GETTING THE MESSAGE LISTSS");
        /*
        -> NEED URL FOR CHAT HISTORY USING (ID:USER+OTHERUSER)
        -> GET ALL CHAT HISTORY AS ARRAY
        -> POPULATE IN THE STATE VARIABLE
        */
        /*
        // METHOD 1: CALL HERE ITSELF.................... */
        const chatkey = this.state.chatkey;    // localStorage.getItem('chatKey');
        // const url = 'chats/' + chatkey + '/chatHistory_' + this.state.user;
        // let ref = firebase.database().ref(url);
      
      
        var storageRef = firebase.storage().ref('chat_storage');
        let currentComponent = this;


        this.state.fbMsgListener.on('value', (snapshot) => {

            currentComponent.setState({
                msgLst: getData(snapshot.val())
            })

            console.log(currentComponent.state.msgLst);
            // UPDATING USER LISTENER ->  OTHERUSER TO NULL
            currentComponent.props.cleanListener(currentComponent.state.userkey, currentComponent.props.chat.otherUser)

        }, (error) => {
            console.log('ERROR IN FETCHING', error)
        })

        console.log('GETTING OUT : COMPO DID MOUNT');
    }


    render() {

        let leftpad = (n) => {
            return n > 9 ? "" + n: "0" + n;
        }
        // POPULATE ALL MESSAGE LISTENING FROM FIREBASE DB
        let msgLst = getData(this.state.msgLst);
        // let msgLst = this.state.msgLst;
        let showTime=null;  
        let chatRoom = msgLst.map((msgs, index) => {
            var date = new Date( msgs.time * 1000);
            let msgTime = date.getDate() + ' '+ month[date.getUTCMonth()];
            let toShowtime= false;
            
            let msgTimeHour = date.getHours() + ':' + leftpad( date.getMinutes() );

           if (showTime !== msgTime){
                toShowtime = true;
                showTime = msgTime; 
            }
            return (<div  key={index}>
                { (toShowtime) ? 
                  ( <span> {'---'+showTime+'---'}  </span> ) 
                  : 
                  (<span></span>  )
                }
                
                <MessageComponent
                msgIsMine={(msgs.by === this.state.user)}
                messageObj={msgs}
                deleteMessage={this.props.deleteMessage}
                />

                <span> 
                    {msgTimeHour}
                </span>  
                   
            </div>
            )

            
        })

        // CONSTAINS SUBCOMP: MESSAGE LIST AND TEXT, SEND BTN
        return (
            <div style={this.state.divStyle}>
                <a href='javascript:void(0)' onClick={() => {
                    // this.state.fbMsgListener.off();
                    this.props.closeChat()
                }}>
                    Close
                </a>
                <h5> {"chat:" + this.props.chat.otherUser} </h5>
                <div>
                    <button disabled={false} onClick={() => {
                        /* 1. need USERCHATKEY USERKEY
                           2. DEL FROM chats/mychatkey/
                           3. DEL from users/userkey/chatlist/otheruser/
                        */
                        let currentComponent = this;
                        let userchat_url = 'chats/' + this.state.chatkey ;
                        let userchalist_url = 'users/' + this.state.userkey + '/chatlist/'+ this.props.chat.otherUser ;

                        firebase.database().ref(userchat_url).set(null);
                        firebase.database().ref(userchalist_url).set(null);

                        this.props.closeChat();
                    }}>
                        DeleteForMe</button>

                 
                </div>

                {chatRoom}

                <form onSubmit={(event) => {
                    event.preventDefault();
                    if (!this.messageValidity()) {
                        return;
                    }
                    this.cleanState();

                    if (!this.state.file) {
                        this.props.sendMsg(
                            cookie.getCookie('auth'),
                            this.props.chat.otherUser,
                            this.props.chat.otherUser_key,
                            this.state.message
                        );
                    } else {
                        this.props.uploadFile(
                            cookie.getCookie('auth'),
                            this.props.chat.otherUser,
                            this.props.chat.otherUser_key,
                            this.state.file,
                            this.state.message
                        );
                    }

                }}>
                    {/* <input type="text" name="msg" id="msgBox" onChange={this.updateMsg} /> */}
                    <textarea id='msgBox' onKeyPress={this.enterpressalert} onChange={this.updateMsg} />
                    <input type="submit" value="Send" />

                    <input type="file" id="fileElement" onChange={this.onChange} />

                </form>

            </div >
        )
    }
}
