import React from "react";

import { FriendComponent } from './friendComponent.js';
import { getData } from './../../utility/helper.js';
import { CookieMap } from './../../utility/cookieLib.js';
const cookie = new CookieMap();

const userName = cookie.getCookie('auth');


export class FriendBoxComponent extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            divStyle: {
                backgroundColor: 'yellow',
                padding: '5%',
                borderStyle: 'groove',
                width: '50%',
                margin: '5%'
            },
            //f_list: getData(props.f_list),
            listComp: null
        }
    }

    // componentDidMount() {
    //     let currentComponent = this;
    // }

    render() {

         let f_list = getData(this.props.f_list);


        // SETSTATE .....

        const listComp = f_list.map((lst, ind) => {
            if (lst.name === userName) return;

            return (<a key={ind} href='javascript:void(0)' onClick={() => {
                 this.props.closeChat();
                this.props.startChat(userName, lst.name, lst.key)  //ADDED OTHERUSER KEY
            }}>
                <FriendComponent  friend={lst.name}> </FriendComponent>
            </a>)
        })

        return (
            <div className='friendBox' style={this.state.divStyle} >
                <p> Friends available </p>
                {listComp}
            </div>
        )
    }
}
