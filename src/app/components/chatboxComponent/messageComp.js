import React from "react";
import { getData } from './../../utility/helper.js'
import { CookieMap } from './../../utility/cookieLib.js';
const cookie = new CookieMap();


export class MessageComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            fileurl: null,
            m: props.messageObj.message
        }
    }

    // LIFECYCLE ...

    componentDidMount() {
        let me = this.state.m;
        let currentComponent = this;

        if (typeof (me) === 'object') {

            var storageRef = firebase.storage().ref('chat_storage');
            storageRef.child('/' + me.metadata.name).getDownloadURL().then(function (url) {
                currentComponent.setState({
                    fileurl: url
                })

            }).catch(function (error) {
                // Handle any errors
                console.log('fetch error', error);
            });
        }

    }


    render() {
        var divStyle = {
            backgroundColor: 'green',
            color: 'white',
            padding: '2%',
        };
        var imgStyle = {
            width: '150px',
            height: '100px'
        };
        var capStyle = {
            color: 'white'
        }

        if (this.props.msgIsMine) divStyle.backgroundColor = 'green';
        else divStyle.backgroundColor = 'blue';


        const closeTab = this.props.msgIsMine ? (
            <a href='javascript:void(0)' onClick={
                () => {
                    this.props.deleteMessage(this.props.messageObj.key);
                }}>
                Close
            </a>
        ) : (
                <span> . </span>
            )

        //const m = this.props.messageObj.message;
        // let urllink=props.filelink;

        let currentState = this.state;

        let isImage = false;
        if (typeof (currentState.m) === 'object') {
            let type = currentState.m.metadata.type;
            if (type.indexOf('image') >= 0) {
                isImage = true;
            }
        }


        return (

            <div style={divStyle}>
                {(typeof (currentState.m) === 'object') ?
                    (currentState.fileurl === null) ?
                        (<span>Loading </span>)
                        : (
                            (isImage) ? (<div>
                                <img style={imgStyle} alt={currentState.m.metadata.name} src={currentState.fileurl} />
                                <span style={capStyle}>{currentState.m.caption}</span>
                            </div>)
                                :
                                (<div>
                                    <a href={currentState.fileurl} target="_blank"> {currentState.m.metadata.name} </a>
                                    <span style={capStyle}>{currentState.m.caption}</span>
                                </div>)
                        )
                    :
                    currentState.m}

                {/* {closeTab} */}

            </div>
        )
    }
}
