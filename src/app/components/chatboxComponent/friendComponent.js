import React from "react";
import { getData } from './../../utility/helper.js'
// import { CookieMap } from './../../utility/cookieLib.js';
// const cookie = new CookieMap();
// const userName = cookie.getCookie('auth');

// export const FriendComponent = (props) => {
//     var divStyle = {
//         color: 'green',
//     };
//     return (
//         <div style={divStyle}>
//             {props.friend}
//         </div>
//     )
// }


export class FriendComponent extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            divStyle: {
                color: 'green',
                margin: '10px'
            },
            NotidivStyle: {
                color: 'red',
                margin: '10px'
            },

            friend: props.friend,
            totmsg: 0
        }
    }

    componentDidMount() {

        let currentComponent = this;

        let userkey = localStorage.getItem('userkey');

        let list_url = 'users/' + userkey + '/listener/' + currentComponent.state.friend;

        let list_ref = firebase.database().ref(list_url);
        list_ref.on('value', (snapshot) => {
            let result = snapshot.val();
            // console.log('Component did mount @ friend comp -> listening to user friend', result);
            if (result) {
                currentComponent.setState({
                    totmsg: result.totmsg,
                })
            } else {
                console.log('listener might not exist or no msg');
            }
        },
            (error) => {
                console.log('ERROr REading other user listner ', error);
            })
    }

    render() {
        return (
            <div style={this.state.divStyle}>
                {/*  {this.props.friend} */}
                {this.state.friend}
                {(this.state.totmsg > 0) ?
                    (<span style={this.state.NotidivStyle}>{this.state.totmsg}</span>)
                    :
                    (<span> {''} </span>)
                }
            </div>
        )
    }
}