import React from "react";
import { getData } from './../utility/helper.js'
import { CookieMap } from './../utility/cookieLib.js';
const cookie = new CookieMap();

import { FriendBoxComponent } from './chatboxComponent/friendboxComponent.js'
import { ChatBoxComponent } from './chatboxComponent/boxComp.js'

export class Chatbox extends React.Component {

    constructor() {
        super();
        this.state = {
            friends: '',
            user: cookie.getCookie('auth')
        }
    }


    componentDidMount() {

        console.log("GETTING USER LIST");
        let ref = firebase.database().ref('/users') ;
        let currentComponent = this;
        ref.on('value', (snapshot) => {
            currentComponent.setState({
                friends: snapshot.val()
            })
        })
    }

    render() {
        const msgList = this.state.friends;


        return (
            <div>
                <div>
                    <FriendBoxComponent
                        f_list={this.state.friends}
                        startChat={this.props.startChat}
                        closeChat={this.props.closeChat}>
                    </FriendBoxComponent>
                </div>
                {}
                {(this.props.chat.boxStatus) ? (
                    <div>
                        <ChatBoxComponent
                            // updateMsg={this.updateMsg}
                            // submitMsg={this.submitMsg}
                            chat={this.props.chat}
                            sendMsg={this.props.sendMsg}
                            closeChat={this.props.closeChat}
                            deleteMessage={this.props.deleteMessage}
                            uploadFile={this.props.uploadFile}
                            cleanListener={this.props.cleanListener}
                            refresh={this.props.refresh}
                        >
                        </ChatBoxComponent>
                    </div>
                ) : (<div> No chat Activated </div>)}
            </div >
        )
    };
}