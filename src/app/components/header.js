import React from "react";
import {NavLink} from 'react-router-dom' ;

import {CookieMap} from './../utility/cookieLib.js';

const cookie = new CookieMap();

const headerStyle = {
    overflow: 'hidden',
    background: '#f1f1f1',
    padding: '20px 10px'
}


const headerAnchorStyle = {
    float: 'left',
  color: 'black',
  text: 'center',
  padding: '12px',
  text_decoration: 'none',
  font_size: '18px', 
  line_height: '25px',
  border_radius: '4px'
}

export const Header = (props) => {
    return (
        <div>
            {/* <div>
                <ul>
                  
                     
                        <li>
                        <NavLink to="/" activeStyle={{color: 'green'}}>Chatbox </NavLink>
                        </li>

                         <li>   
                        <u> User: {cookie.getCookie('auth')} </u> 
                        </li>

                        <li>
                        <a href="#" onClick={() => {
                            props.logout();
                            cookie.deleteCookie("auth");
                            localStorage.clear();
                            window.location.pathname = "/";
                        }}> Logout
                        </a>
                        </li>

                   
                </ul>
            </div> */}

                <div style={headerStyle} className="header">
                <div className="header-right">
    
                        <NavLink style={headerAnchorStyle} to="/" activeStyle={{color: 'green'}}>Chatbox </NavLink>
                           
                        <u style={headerAnchorStyle}> User: {cookie.getCookie('auth')} </u> 
                       

                        <a href="#" onClick={() => {
                            props.logout();
                            cookie.deleteCookie("auth");
                            localStorage.clear();
                            window.location.pathname = "/";
                        }}> Logout
                        </a>

                </div>
                </div>


        </div>
    );
}