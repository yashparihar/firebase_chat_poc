import React from "react";
import { BrowserRouter as Router, Link, NavLink, Redirect, Prompt } from 'react-router-dom';
import { CookieMap } from './../utility/cookieLib.js';
import { getData, pushInFbDb } from './../utility/helper.js'

const cookie = new CookieMap();
var b = document.getElementById('login_btn');
// DONT WORK
const goHome = () => {
    <Redirect to="/home" />
}


export class Login extends React.Component {

    constructor() {
        super();
        this.state = {
            users: [],
            divStyle: {
                margin: '10%',
                width: '50%',
                border: '3px solid black',
                padding: '10px'
            },
            HeadStyle: {
                margin: '10%',
                width: '50%',
                // border: '3px solid black',
                padding: '10px'
            }
        }
    }


    validateField() {

        return new Promise((resolve, reject) => {
            var u = document.getElementById("username").value;
            var p = 'password';
            console.log("U:" + u);
            if (u === null || u === '' )   reject('Cant keep blank')

            /*
                 1 . userid validation -> trim , not-case-sensitive , no special character , only alphabetics character
                 2 . 
           */
           
           u = u.trim();

            var letters = /^[A-Za-z]+$/;
            if(!u.match(letters)){
                reject('Only alphabet allowed')
            }
            


            // console.log("before login, its status: " + this.props.loginStatus);//.then((state)=>{console.log(state)}));
            try {
                resolve(this.props.loginCheck(u, p));
            }
            catch (err) {
                console.log(err);
                reject(err);
            }
        });
    }

    validateUser(username){
        console.log(username);
    }

    setUserToFbDb() {
        this.validateField().then((res) => {
            if (!this.props.loginStatus) {
                alert('something was wrong'); document.getElementById('login_btn').disabled = false;
                return;
            }
            // console.log("yeh login success: " + this.props.loginStatus);

            // console.log("login status response: " + res);
            var userVal = document.getElementById('username').value.trim();
            // console.log(userVal);

            //CHECK IF THIS USER EXIST IN FIREBASE ELSE ADD IN
            let ref = firebase.database().ref('/users');
            ref.once('value', (snapshot) => {
                this.setState({
                    users: getData(snapshot.val())
                })

                // console.log("from firebase userlist :", snapshot.val());
                // console.log("from state userlist :", this.state.users.length);

                let userDetail = this.state.users.filter(li => {
                    return (li.name === userVal);
                })

                if (userDetail.length > 0) {

                    localStorage.setItem('userkey', userDetail[0].key),
                        cookie.setCookie("auth", userVal, 365);
                    window.location.pathname = "/";
                }
                // if (this.state.users.filter(li => li.name === userVal).length > 0 && this.state.users.length > 0) {
                //     //   console.log(userVal + ' is already there');
                //     cookie.setCookie("auth", userVal, 365);
                //     // window.location.pathname = "/";    
                // }
                else {
                    // let ischatkey = false;
                    // let isuserkey = true;

                    pushInFbDb({ name: userVal }, '/users' , 'userkey')
                        .then(() => {
                            cookie.setCookie("auth", userVal, 365);
                            window.location.pathname = "/";
                        })
                        .catch((err) => reject(err))
                }
            })
        })
            .catch((err) => {
                alert( err); document.getElementById('login_btn').disabled = false;
                console.log("err is " + err);
            })
    }


    render() {
        return (

            <div>
                <h2 style={this.state.HeadStyle}> CHATTING DEMO APPLICATION </h2>
            <div style={this.state.divStyle}>
            
            <p> Enter Any Username</p>
            <input type="text" id="username" name="username" placeholder="username" />
                {/* <input type="password" name="password" id="password" /> */}

                <button id="login_btn" onClick={() => {
                    var b = document.getElementById('login_btn');
                    var u = document.getElementById('username');

                    b.disabled = true;

                    

                    this.setUserToFbDb();
                }
                }>Login
                </button>
            </div>
            </div>
        )



    };
}